************************************************************************
Announcements
************************************************************************

30 April 2019
========================================================================

* Information about the homework logistics was added to the ``README.rst`` file 


29 April 2019
========================================================================

* Contrary to previous announcements, there will be a lecture next week, 07 May 2019
* Starting 30 Apr 2019, the lecture will take place in room N3130
