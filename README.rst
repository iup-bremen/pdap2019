***********************************
Practical Data Analysis with Python
***********************************

:Author: Dr. Andreas Hilboll
:Email:  hilboll@uni-bremen.de
:Date:   Summer term 2019

This Git repository holds the material for the course `Practical Data
Analysis with Python
<https://elearning.uni-bremen.de/dispatch.php/course/members/index?cid=a679b8f514ba45a2201abce2462da653>`__
by `Dr. Andreas Hilboll <http://www.iup.uni-bremen.de/~hilboll>`__,
given at the `University of Bremen <http://www.uni-bremen.de/>`__
during summer term 2019.


General Information
=====================

Time
  Tuesday, 08:15 - 09:45

Dates
  02 Apr 2018 - 09 Jul 2018

Location
  `NW1 / N3130 <http://oracle-web.zfn.uni-bremen.de/lageplan/lageplan?haus=NW1&raum=n3130&pi_anz=0>`__

This course aims to be a very practical introduction to using a
computer in scientific data analysis, using the `Python
<https://www.python.org/>`__ programming language. The course will be
most beneficial to second semester students, as they will be able to
directly apply the newly learned techniques in their subsequent
M.Sc. thesis work.

The course will be accessible to all students, as it will not make any
assumptions on the students' computer environment, i.e., all topics will be
explained for all relevant operating systems (Windows, MacOS, Linux).

**Participants are expected to bring their own laptops, as the course
contains large parts of practical work.**


Links
=====

:Course repository: http://unihb.eu/pdap2019
:JupyterLab server: http://unihb.eu/lamoslab


Flipped classroom
=================

This course is organized as a `flipped classroom
<https://en.wikipedia.org/wiki/Flipped_classroom>`__.  This means that
there will be *preparatory* homework each week --- I expect you to
come prepared so that we can spend the course hours in a meaningful
way.


Syllabus
========

The first part of the course will touch on the following subjects:

- *Jupyter Notebook*: interactive, web-based usage of Python
- Getting started: How to setup your own computer for data analysis in Python.
- Hands-on introduction to the Python scientific ecosystem: Arrays and
  mathematical operations.
- Labeled arrays, or how to intuitively work with data.
- Reading and writing data in common file formats.
- Notebooks, script, and modules: How to properly organize your code
- *But this worked yesterday, before I made some changes ...*, or: an
  introduction to version control.
- Making both beautiful and meaningful plots from data.
- An overview of the most common special-topic libraries for all research areas
  covered by the pep_ program.

.. _pep: http://www.pep.uni-bremen.de/

In its last sessions, the course will focus on a practical introduction to the
most common data analysis tasks, like, among others, curve fitting, parameter
estimation, and correlation analysis.

Every week, there will be 2 hours of course (approx. 1 hour lecture + 1 hour
practical exercises). There will be weekly homework excercises, plus two graded
homework projects.


(Weekly) homework
=================

50% of the weekly homework exercises have to be completed in order to complete the "Studienleistung" (which you need in order to get awared CPs for this course).

Submission of the homework is done automatically:

1. Create a folder ``/home/pdap2019/YOURUSERNAME/homework`` on https://aether.uni-bremen.de/lamoslab
2. Create a folder ``01`` (or ``02``, and so on) inside that folder.
3. Create a folder ``submission`` inside that folder.
4. Save your homework notebook in that folder ``/home/pdap2019/YOURUSERNAME/homework/01/submission`` (or ``.../homework/02/submission``, or ...).  There must be *only one* ``*.ipynb`` file in that folder.  **Otherwise, your submission is invalid!**
5. All homework will be collected automatically on the due date at 08:00 am.  Late submissions are not possible.  Submission by any other means is not possible.

You can check if your submission is in the correct place as follows:

1. Log in to our JupyterLab
2. Open a "Terminal" (that's the icon with the ``$_`` sign, on the bottom, in the *Other* section)
3. Run the command ``pdap2019_check_submission``
4. Don't forget to close the terminal again by typing ``exit``
