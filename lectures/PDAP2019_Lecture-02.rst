**********************************************************
Lecture 02: First steps in Python: variables, lists, loops
**********************************************************

:Author:       Dr. Andreas Hilboll
:E-mail:       hilboll@uni-bremen.de
:Date:         09 Apr 2019
:Course:       Practical Data Analysis with Python
:Institution:  University of Bremen


Lecture notes
=============

see file ``PDAP2019_Lecture-02_Transcript.ipynb`` in this directory


Reading assignment
==================

We will continue working with the course `Programming with Python
<https://swcarpentry.github.io/python-novice-inflammation/>`__ by
`software carpentry <https://software-carpentry.org/>`__.

Until next week, please work through the following chapters:

#. `Analyzing Data from Multiple Files
   <https://swcarpentry.github.io/python-novice-inflammation/04-files/index.html>`__
#. `Making Choices
   <https://swcarpentry.github.io/python-novice-inflammation/05-cond/index.html>`__
#. `Creating Functions
   <https://swcarpentry.github.io/python-novice-inflammation/06-func/index.html>`__


Installing Python on your computer
==================================

Please try to install Anaconda Python on your own computer.
Instructions can be found in the file ``INSTALL.rst`` in this
repository.

You are welcome to start working with your own computer at any time.
Starting mid-May, there will be assignments for which you *need* to
have Python installed on your own computer.

In this course I assume that you are using the Anaconda Python
installation as I describe in the ``INSTALL.rst`` file.  During the
semester, we might install additional packages / modules.

Of course, you are welcome to use any other distribution of Python on
your computer.  However, please note that I will *not* be able to help
you with any problems in that case.
