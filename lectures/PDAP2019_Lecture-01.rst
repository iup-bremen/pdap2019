************************************
Lecture 01: Introduction to PDAP2019
************************************

:Author:       Dr. Andreas Hilboll
:E-mail:       hilboll@uni-bremen.de
:Date:         02 Apr 2019
:Course:       Practical Data Analysis with Python
:Institution:  University of Bremen


Introduction
============

Who am I?
---------

-  Andreas Hilboll
-  working at IUP (`Institute of Environmental
   Physics <http://www.iup.uni-bremen.de/>`__)
-  past: PhD in atmospheric remote sensing
-  now: PostDoc in atmospheric modeling / `LAMOS
   group <http://www.iup.uni-bremen.de/lamos/>`__, Prof. Vrekoussis
-  when you need to contact me:

   +-----------------------------------+-----------------------------------+
   | E-mail                            | hilboll@uni-bremen.de             |
   +-----------------------------------+-----------------------------------+
   | Room                              | `S3132 <http://oracle-web.zfn.uni |
   |                                   | -bremen.de/lageplan/lageplan?pi_r |
   |                                   | aum_id=8251&pi_raumnummer=S3121&p |
   |                                   | i_anz=1>`__                       |
   |                                   | (formerly S3121)                  |
   +-----------------------------------+-----------------------------------+
   | Phone                             | +49(0)421-218-62133               |
   +-----------------------------------+-----------------------------------+

Who are you?
------------

-  Please fill in the participant list.
-  Is there anything in particular you want to learn here?
-  What is your previous experience with Python?
-  What is your previous experience with version control?

Course overview
===============

.. _introduction-1:

Introduction
------------

This course is a *practical* introduction to using Python for data
analysis. This means that

-  we start by learning about the *Jupyter Notebook* (*today*)
-  we discuss the basics of the Python programming language (*next
   week*)
-  you will install Python on your computer

Python science basics
---------------------

After that (*April–early May*) you will learn about

-  operations with arrays
-  reading and writing data in common file formats
-  labeled arrays, or how to intuitively work with data
-  making beautiful and meaningful plots from data

Python toolbox
--------------

On the technical side, we will

-  learn about the differences between interactive notebooks, scripts
-  start organizing our code in modules for easy re-use
-  learn about automatic version control with Git: What is it, why
   should we use it, and how do we do it?

Applications
------------

Starting in *June*, we will take an in-depth look at common data
analysis problems like

-  curve fitting
-  minimization of a cost function
-  parameter estimation
-  correlation analysis

Also, we can look into special topic libraries which *you* will want to
work with (let me know which topics interest you!)

This course is *for you*
------------------------

The goal of this course is to **help you learn Python**. I expect you to
work independently with the available online material.

This course is meant to be *practical* to *you*. So you are welcome to
let me know what you would like to learn!

Course Logistics
================

Time of this lecture
--------------------

At what time should this course take place?

-  08.15 – 09.45

*or*

-  08.30 – 10.00

**Your opinion?**

Replacement day for 07 May 2019
-------------------------------

I will not be in Bremen on 07 May.

When should we do that lesson?

**Suggestions?**

Communication
-------------

**Course homepage:** `unihb.eu/pdap2019 <http://unihb.eu/pdap2019>`__

- I will use `Stud.IP <https://elearning.uni-bremen.de/>`__ to make
  announcements etc. So make sure that you register for this course
- If you need to contact me, drop me an `e-mail
  <mailto:hilboll@uni-bremen.de>`__ or come by my office S3132
- All course material will be uploaded to the Git repository at
  https://gitlab.com/iup-bremen/pdap2019

Reading assignments
-------------------

There are weekly reading assignments introducing new concepts.

Sometimes I might prepare a video for you to watch, sometimes I'll
give you some resource (document, blog article, paper, …) to read.

I expect you to come prepared to the course. We will have the
opportunity to discuss any questions you might have about the week's
assignment during the class.

Homework
--------

In addition, in most weeks, there will be a small exercise to complete
at home. Completion of at least half of these small exercises is
considered to be your *Studienleistung* (*course performance*), so it
is a prerequisite for getting CPs!

Exam / grades
-------------

There will be 2 graded homework projects:

-  3–4 weeks time to complete
-  to be completed in groups of 1–3 students
-  *examination performance* will be arithmetic average of the two
   homework assignments

Reading material
================

Reading material is available in the file `REFERENCES.rst
<../REFERENCES.rst>`__ in the course repository. This file will be
continuously updated as the semester goes on.

Reading assigment: The JupyterLab Interface
===========================================

https://jupyterlab.readthedocs.io/en/latest/user/interface.html

Practical excercise: First steps in the notebook
================================================

#. Go to `unihb.eu/lamoslab <http://unihb.eu/lamoslab>`__
#. Login
#. Work through
   https://swcarpentry.github.io/python-novice-gapminder/01-run-quit/ , start at *The Notebook has Command and Edit modes*

   (**Note:** That text is written for the old Jupyter Notebook; things
   will look a bit different in our system)

Reading assignment
==================

For learning the basics of Python, we will work with the course
`Programming with
Python <https://swcarpentry.github.io/python-novice-inflammation/>`__ by
`software carpentry <https://software-carpentry.org/>`__.

Until next week, please work through the following chapters:

#. `Analyzing Patient
   Data <https://swcarpentry.github.io/python-novice-inflammation/01-numpy/index.html>`__
#. `Repeating Actions with
   Loops <https://swcarpentry.github.io/python-novice-inflammation/02-loop/index.html>`__
#. `Storing Multiple Values in
   Lists <https://swcarpentry.github.io/python-novice-inflammation/03-lists/index.html>`__
